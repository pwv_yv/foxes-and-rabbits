package io.muic.ooc.fab;

import java.util.List;
import java.util.Random;

public abstract class Animal {
    // Random generator
    private static final Random RANDOM = new Random();

    // Individual characteristics of an individual animal (instance fields).
    // The animal's age.
    private int age;
    // Whether the animal is alive or not.
    private boolean alive;
    // The animal's position.
    private Location location;
    // The field occupied.
    private Field field;

    protected Animal(boolean randomAge, Field field, Location location) {
        this.age = 0;
        this.alive = true;
        this.field = field;
        setLocation(location);
        if (randomAge) {
            setAge(RANDOM.nextInt(getMaxAge()));
        }
    }

    protected abstract int getMaxAge();

    protected abstract int getBreedingAge();

    protected abstract double getBreedingProbability();

    protected abstract int getMaxLitterSize();

    protected abstract void act(List<Animal> newAnimals);

    /**
     * Set animal's current age
     *
     * @param age New age of the animal.
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Set animal to be alive or dead.
     *
     * @param alive New alive status.
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    /**
     * Indicate that the animal is no longer alive. It is removed from the field.
     */
    protected void setDead() {
        setAlive(false);
        if (location != null) {
            field.clear(location);
            location = null;
            field = null;
        }
    }

    /**
     * Place the fox at the new location in the given field.
     *
     * @param newLocation The fox's new location.
     */
    protected void setLocation(Location newLocation) {
        if (location != null) {
            field.clear(location);
        }
        location = newLocation;
        field.place(this, newLocation);
    }

    /**
     * Check whether the fox is alive or not.
     *
     * @return True if the fox is still alive.
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * Return position of animal on the field.
     *
     * @return The Animal's location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Return the field the animal is on.
     *
     * @return The field the animal is associated with.
     */
    public Field getField() {
        return field;
    }

    /**
     * Increase the age. This could result in the fox's death.
     */
    protected void incrementAge() {
        age++;
        if (age > getMaxAge()) {
            setDead();
        }
    }

    /**
     * Move animal to a new location if possible. If not possible the animal dies from overcrowding.
     */
    protected void move(Location newLocation) {
        // Try to move into a free location.
        if (newLocation != null) {
            setLocation(newLocation);
        } else {
            // Overcrowding.
            setDead();
        }
    }

    /**
     * An animal can breed if it has reached the breeding age.
     *
     * @return true if the animal can breed, false otherwise.
     */
    protected boolean canBreed() {
        return age >= getBreedingAge();
    }

    /**
     * Generate a number representing the number of births, if it can breed.
     *
     * @return The number of births (may be zero).
     */
    protected int breed() {
        int births = 0;
        if (canBreed() && RANDOM.nextDouble() <= getBreedingProbability()) {
            births = RANDOM.nextInt(getMaxLitterSize()) + 1;
        }
        return births;
    }

    /**
     * Check whether or not this animal is to give birth at this step. New
     * births will be made into free adjacent locations.
     *
     * @param newAnimals A list to return newly born animals.
     */
    protected void giveBirth(List<Animal> newAnimals, AnimalType animalType) {
        // New animals are born into adjacent locations.
        // Get a list of adjacent free locations.
        List<Location> free = field.getFreeAdjacentLocations(location);
        int births = breed();
        for (int b = 0; b < births && free.size() > 0; b++) {
            Location loc = free.remove(0);
            Animal young = AnimalFactory.createAnimal(animalType, false, field, loc);
            newAnimals.add(young);
        }
    }
}
