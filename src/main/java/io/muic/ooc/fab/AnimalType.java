package io.muic.ooc.fab;

import java.awt.*;

public enum AnimalType {

    FOX(Fox.class, Color.BLUE, 0.02),
    RABBIT(Rabbit.class, Color.ORANGE, 0.08);


    private Class<? extends Animal> animalClass;

    private Color color;

    private double probability;

    AnimalType(Class<? extends Animal> animalClass, Color color, double probability) {
        this.animalClass = animalClass;
        this.color = color;
        this.probability = probability;
    }
    // The class of the animal type.
    public Class<? extends Animal> getAnimalClass() {
        return animalClass;
    }
    // The color of cell occupied by the animal type.
    public Color getColor() {
        return color;
    }
    // The probability that an animal type will be created in any given grid position.
    public double getProbability() {
        return probability;
    }
}
